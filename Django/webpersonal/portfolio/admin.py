from django.contrib import admin
from .models import Project
# Register your models here.

# Esta clase sirve para mostrar los campos created y updated en el panel de administrador
class ProjectAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')


admin.site.register(Project,ProjectAdmin)